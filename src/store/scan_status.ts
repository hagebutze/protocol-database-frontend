import { readable } from 'svelte/store';
import { SUBSCRIBE_SCAN_STATUS_CHANGED, GET_SCAN_STATUS } from '../services/graphql/queries'
import { Client } from '../services/graphql/client'


export class ScanStatus {
    status = "unknown";
    message = "status not fetched from server";
    numProtocolsFound = 0;
    numProtocolsScanned = 0;
    numProtocolsFailed = 0;
}

export const scanStatus = readable(new ScanStatus(), function start(set) {
    // Get initial status
    Client.query({
        query: GET_SCAN_STATUS
    }).then((result) => {
        set(result.data.scanStatus)
    }).catch((error) => {
        let status = new ScanStatus()
        status.status = "error"
        status.message = `error fetching status: ${error}`
        set(status)
    })
    // Now subscribe to follup queries
    Client.subscribe({
        query: SUBSCRIBE_SCAN_STATUS_CHANGED,
        }).subscribe({
            next(status) {
                set(status.data.scanStatusChanged)
            },
            error(error) {
                let status = new ScanStatus()
                status.status = "error"
                status.message = `error fetching status: ${error}`
                set(status)
            },
            complete() {
                console.log("complete")
            }
        })

})

