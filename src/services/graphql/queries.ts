import {gql} from "apollo-boost";
import moment from "moment";

export const GET_SCAN_STATUS = gql`query {
    scanStatus {
        status,message,numProtocolsFound,numProtocolsScanned,numProtocolsFailed
    }
}`

export const SUBSCRIBE_SCAN_STATUS_CHANGED = gql`subscription {
    scanStatusChanged {
        status,message,numProtocolsFound,numProtocolsScanned,numProtocolsFailed
    }
}`

export const START_SCAN = gql`mutation {
  startScan {
      status
  }  
}`

export const MONTHS_WITH_PROTOCOLS = gql`query {
    monthsWithProtocols {
        year
        month
        numProtocols
    }
}`

export function PROTOCOLS(minDate: Date, maxDate: Date) {
    return gql`query {
        protocolQuery(limit: 30, minDate: "${moment(minDate).format('YYYY-MM-DD')}", maxDate: "${moment(maxDate).format('YYYY-MM-DD')}") {
            protocols {
                type
                date
                id
                hasErrors
                error
            }
        }
    }`
}

export function PROTOCOL(id: string) {
    return gql`query {
        protocol(id: ${id}) {
            id
            date
            type
            nextcloudLink
            hasErrors
            error
            chapters {
                id
                headline
                text
                startPage
                endPage
                startPosition
                endPosition
            }
            consensuses {
                id
                text
                startPage
                endPage
                startPosition
                endPosition
            }
            decisions {
                id
                text
                startPage
                endPage
                startPosition
                endPosition
            }
        }
    }`
}

export function DECISIONS(minDate: String, maxDate: String) {
    let minDatePara = ""
    let maxDatePara = ""
    if (minDate != "") {
        minDatePara = `, minDate: "${minDate}"`
    }
    if (maxDate != "") {
        maxDatePara = `, maxDate: "${maxDate}"`
    }
    return gql`query {
        decisions(limit: 100${minDatePara}${maxDatePara}) {
            totalCount
            decisions {
                protocol_id
                date
                nextcloudLink
                id
                text
                startPage
                endPage
                startPosition
                endPosition
            }
        }
    }`
}

export function CONSENSUSES(minDate: String, maxDate: String) {
    let minDatePara = ""
    let maxDatePara = ""
    if (minDate != "") {
        minDatePara = `, minDate: "${minDate}"`
    }
    if (maxDate != "") {
        maxDatePara = `, maxDate: "${maxDate}"`
    }
    return gql`query {
        consensuses(limit: 100${minDatePara}${maxDatePara}) {
            totalCount
            consensuses {
                protocol_id
                date
                nextcloudLink
                id
                text
                startPage
                endPage
                startPosition
                endPosition
            }
        }
    }`
}
