// Remove the apollo-boost import and change to this:
import ApolloClient from "apollo-client";
// Setup the network "links"
import { WebSocketLink } from 'apollo-link-ws';
import { HttpLink } from 'apollo-link-http';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';

const httpLink = new HttpLink({
    uri: "__api_http_url".replace("{hostname}", location.host),
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
    uri: "__api_ws_url".replace("{hostname}", location.host),
    options: {
        reconnect: true
    }
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
    // split based on operation type
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query);
        return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpLink,
);

// Create a instance of apollo to use the same base url.
export const Client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
    // onError: ({ networkError, graphQLErrors }) => {
    //     console.log("graphQLErrors", graphQLErrors);
    //     console.log("networkError", networkError);
    // },
});
