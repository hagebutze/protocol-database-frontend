import App from './App.svelte';
import "./scss/bootstrap.scss";
import "./scss/variables.scss";

const app = new App({
	target: document.body,
	props: {}
});

export default app;

